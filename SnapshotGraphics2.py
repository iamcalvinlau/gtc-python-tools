# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

# Snapshot Module
import math
import numpy
import matplotlib.pyplot as plt
import scipy
import matplotlib.gridspec as gridspec

# Definitions cell! Run first!
############################
############################
############################
# readsnapshot:
#
# fname is the name of the snapshot file you want to open, use a string
# kind is what type of files you want to read with 0 being default
#   0: read all data kinds-->broken, don't use
#   1: read only profile
#   2: read only pdf
#   3: read only poloidata
#   4: read only fluxdata
###############################
def readsnapshot(fname,kind=0): 
    """
    This reads snap.out files into something that can be read by python more easily.
    kind=0 reads all data
    kind=1 reads just profile
    kind=2 reads just pdf
    kind=3 reads just poloidal
    kind=4 reads just flux
    """
  #try:  
    lines = open(fname, 'r').readlines()
    nspecies=int(lines[0])  # of species: ion, electron, EP, impuries
    nfield=int(lines[1])    # of field variables: phi, a_para, fluidne
    nvgrid=int(lines[2])    # of grids in energy and pitch
    mpsi=int(lines[3])      # of radial grids: mpsi+1
    mtgrid=int(lines[4])    # of poloidal grids
    mtoroidal=int(lines[5]) # of toroidal (parallel) grids
    tmax=float(lines[6])    #upper bound of temperature
    #print "nspecies=",nspecies,",","nfield=",nfield,",","nvgrid=",nvgrid,",","mpsi=",mpsi,",","mtgrid=",mtgrid,",","mtoroidal=",mtoroidal,",","tmax=",tmax

    sect1=mpsi*6*nspecies
    sect2=nvgrid*4*nspecies
    sect3=mtgrid*mpsi*(nfield+2)
    sect4=mtgrid*mtoroidal*nfield

    profileseries=numpy.zeros(sect1,dtype=float)
    pdfseries=numpy.zeros(sect2,dtype=float)
    poloidataseries=numpy.zeros(sect3,dtype=float)
    fluxdataseries=numpy.zeros(sect4,dtype=float)
    
    profile=numpy.zeros((mpsi,6,nspecies),dtype=float)
    pdf=numpy.zeros((nvgrid,4,nspecies),dtype=float)
    poloidata=numpy.zeros((mtgrid,mpsi,nfield+2),dtype=float)
    fluxdata=numpy.zeros((mtgrid,mtoroidal,nfield),dtype=float)

    ## this separates the files into the four sections as a series of float numbers
    if kind==0 or kind==1:
        for i in range(0,sect1):
            profileseries[i]=float(lines[7+i])
    if kind==0 or kind==2:
        for i in range(0,sect2): 
            pdfseries[i]=float(lines[7+sect1+i])
    if kind==0 or kind==3:
        for i in range(0,sect3):
            poloidataseries[i]=float(lines[7+sect1+sect2+i])
    if kind==0 or kind==4:
        for i in range(0,sect4): 
            fluxdataseries[i]=float(lines[7+sect1+sect2+sect3+i])
        
    ## this reorganizes the four sections into the data format we expect in idl
    if kind==0 or kind==1:
        for i in range(0,nspecies):
            for j in range(0,6):
                for k in range(0,mpsi):
                    profile[k,j,i]=profileseries[k+(j*mpsi)+(i*mpsi*6)]
                    
    if kind==0 or kind==2:
        for i in range(0,nspecies):
            for j in range(0,4):
                for k in range(0,nvgrid):
                    pdf[k,j,i]=pdfseries[k+(j*nvgrid)+(i*nvgrid*4)]
                    
    if kind==0 or kind==3:            
        for i in range(0,nfield+2):
            for j in range(0,mpsi):
                for k in range(0,mtgrid):
                    poloidata[k,j,i]=poloidataseries[k+(j*mtgrid)+(i*mpsi*mtgrid)]
    
    if kind==0 or kind==4:    
        for i in range(0,nfield):
            for j in range(0,mtoroidal):
                for k in range(0,mtgrid):
                    fluxdata[k,j,i]=fluxdataseries[k+(j*mtgrid)+(i*mtgrid*mtoroidal)]             
    if kind==0:
        return (profile, pdf, poloidata,fluxdata,nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)
    elif kind==1:
        return (profile,nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)
    elif kind==2:
        return (pdf,nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)
    elif kind==3:
        return (poloidata,nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)
    elif kind==4:
        return (fluxdata,nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)
  #except:
    #print "Something went wrong!"
    #return
#########################################################################################################
## radial plot
###########################
def radialprof(f,mpsi,mtgrid,plott=0,dots=1):
    """
    this is a translation of the idl pro
    """
    x=range(0,mpsi)
    #y1=numpy.zeros(mpsi)
    y2=numpy.zeros(mpsi)
    
    for i in range(0,mpsi-1):
        y2[i]=f[0,i]
    if plott==0:
        ymin=min(y2)
        ymax=max(y2)
        xmin=min(x)
        xmax=max(x) 
        if dots==0:
    	    plt.plot(x,y2,'o-')
        elif dots!=0:
    	    plt.plot(x,y2)
    	plt.ylim(ymin,ymax)
    	plt.xlim(xmin,xmax)
    elif plott!=0:
        plt.plot(x,y2)
        plt.ylim(0,plott)
    return y2
#########################################################################################################
## radial plot
###########################
def radial(f,mpsi,mtgrid,plott=0,dots=1):
    """
    this is a translation of the idl pro
    """
    x=range(0,mpsi)
    #y1=numpy.zeros(mpsi)
    y2=numpy.zeros(mpsi)
    
    for i in range(0,mpsi-1):
        #y1[i]=f[0,i]
        for j in range(0,mtgrid-1):
            y2[i]=y2[i]+(f[j,i]*f[j,i])
        ytf=y2[i]/mtgrid
        y2[i]=math.sqrt(ytf)  
    if plott==0:
        ymin=min(y2)
        ymax=max(y2)
        xmin=min(x)
        xmax=max(x) 
        if dots==0:
    	    plt.plot(x,y2,'o-')
        elif dots!=0:
    	    plt.plot(x,y2)
    	plt.ylim(ymin,ymax)
    	plt.xlim(xmin,xmax)
    elif plott!=0:
        plt.plot(x,y2)
        plt.ylim(0,plott)
    return y2
#########################################################################################################
## poloidal profile
###########################
def polprofile(f,mpsi,mtgrid,plott=0,dots=1):
    """
    this is a translation of the idl pro
    """
    x=range(0,mtgrid)
    #y1=numpy.zeros(mtgrid)
    y2=numpy.zeros(mtgrid)
    
    for i in range(0,mtgrid-1):
        #y1[i]=f[i,mpsi/2]
        for j in range(0,mpsi-1):
            y2[i]=y2[i]+(f[i,j]*f[i,j])
        ytf=y2[i]/mpsi
        y2[i]=math.sqrt(ytf)
    if plott==0:
        ymin=min(y2)
        ymax=max(y2)
        xmin=min(x)
        xmax=max(x) 
        if dots==0:
    	    plt.plot(x,y2,'o-')
        elif dots!=0:
    	    plt.plot(x,y2)
    	plt.ylim(ymin,ymax)
    	plt.xlim(xmin,xmax)
    elif plott!=0:
        plt.plot(x,y2)
        plt.ylim(0,plott)
    return y2
#########################################################################################################
## poloidal plot phi
####################
def poloidal(x,y,f,ncolor=12,contourlines="n",contourlevels=12,contourcolors="k"):
    """
    this is a translation of the idl pro
    """
    
    #ncolor=12
    length=1.0
    x=x*length
    y=y*length
    f=f
    
    zmax=abs(f).max()
    zmin=-abs(f).max()
    xmax=x.max()
    xmin=x.min()
    ymax=y.max()
    ymin=y.min()
  
    flevel=[]  
    clevel=[]
    for j in range(0,ncolor):
        flevel.append(zmin+((zmax-zmin)*j)/(ncolor-1))
    for j in range(0,contourlevels):
        clevel.append(zmin+((zmax-zmin)*j)/(contourlevels-1))
    #plt.figure(figsize=(26,10))
    #plt.subplot(1,2,1)
    plt.contourf(x,y,f,flevel)
    if contourlines=="y":
	plt.hold("True")
        if contourcolors=="k":
    		plt.contour(x,y,f,clevel,colors='k')
        else:
    		plt.contour(x,y,f,clevel)
        plt.hold("False")
    plt.xlim([xmin,xmax])
    plt.ylim([ymin,ymax])
    return
#########################################################################################################
## plot flux lines
####################
def plot_flux_lines(y,x,i,thetalines,psilines,mpsi,style='k',alphaness=0.5):
    yy=y[i]
    xx=x[i]
    #thetalines=16
    #psilines=7


    
    thetadiff=len(yy)/thetalines
    for j in xrange(thetalines):
        plt.plot(yy[j*thetadiff],xx[j*thetadiff],style,alpha=alphaness)
    #plot(yy[mtoroidal[i]-1],xx[mtoroidal[i]-1],'k')
    yy=zip(*y[i])
    xx=zip(*x[i])
    
    psidiff=len(yy)/psilines
    for j in xrange(psilines):
        plt.plot(yy[psidiff*j],xx[psidiff*j],style,alpha=alphaness)
    plt.plot(yy[mpsi[i]-1],xx[mpsi[i]-1],style,alpha=alphaness)
#########################################################################################################
## flux surface 
####################
def fluxdata(f,mtoroidal,mtgrid,ncolor=12,contourlines="n",contourlevels=12,contourcolors="k",toroidaln=1):
    """
    this is a translation of the idl pro
    """
    
    #ncolor=12
    length=1.0
    f=f
    x=[]
    y=[]
    for i in xrange(mtgrid):
	    y.append(2.0*3.141592*float(i)/float(mtgrid))
    if(toroidaln==1):
        for i in xrange(mtoroidal):
	    x.append(2.0*3.141592*float(i)/float(mtoroidal*toroidaln))
    else:
       for k in xrange(toroidaln):
          xtmp=[]
       	  for i in xrange(mtoroidal):
	    xtmp.append(2.0*3.141592*float(i+(k*mtoroidal))/float(mtoroidal*toroidaln))
          x.append(xtmp)
       #x=zip(*x)
    ff=f#zip(*f)#numpy.zeros((mtoroidal,mtgrid))
    #ff=numpy.zeros((mtoroidal*toroidaln,mtgrid))
    
    #for k in xrange(toroidaln):
    #	for i in xrange(mtoroidal):
    #		for j in xrange(mtgrid):
    #                indy=i+(k*mtoroidal) 
    #		    ff[indy][j]=f[i][j]

    zmax=abs(f).max()
    zmin=-abs(f).max()
    xmax=max(x)
    xmin=min(x)
    ymax=max(y)
    ymin=min(y)
    
    #print len(x), len(y), len(f[0]), len(f)

    flevel=[]  
    clevel=[]
    for j in range(0,ncolor):
        flevel.append(zmin+((zmax-zmin)*j)/(ncolor-1))
    for j in range(0,contourlevels):
        clevel.append(zmin+((zmax-zmin)*j)/(contourlevels-1))
    #plt.figure(figsize=(26,10))
    #plt.subplot(1,2,1)
    if toroidaln==1:
       plt.contourf(x,y,ff,flevel)
    else:
       plt.hold("True")
       for k in xrange(toroidaln):
           plt.contourf(x[k],y,ff,flevel)
    if contourlines=="y":
	plt.hold("True")
        if contourcolors=="k":
    		plt.contour(x,y,ff,clevel,colors='k')
        else:
    		plt.contour(x,y,ff,clevel)
        plt.hold("False")
    if toroidaln==1:
    	plt.xlim([xmin,xmax])
    else:
        plt.xlim([0,2*3.141592])
    plt.ylim([ymin,ymax])
    return
#########################################################################################################
## flux surface2 
####################

def fluxdata2(f,mtoroidal,mtgrid,ncolor=12,contourlines="n",contourlevels=12,contourcolors="k",nofill='y',toroidaln=1):
    """
    this is a translation of the idl pro
    """
    import matplotlib.ticker as ticker
    length=1.0
    #ff=f
    x=[]
    y=[]
    ff=[]
    for i in xrange(mtgrid/2+1,mtgrid):
        ff.append(f[i])

    for i in xrange(0,mtgrid/2+1):
        ff.append(f[i])
    #ff[0:mtgrid/2]=f[mtgrid/2+1:mtgrid]
    #ff[mtgrid/2+1:mtgrid]=f[0:mtgrid/2]
    for i in xrange(mtgrid):
	    y.append((2*3.141592*float(i)/float(mtgrid))-3.141592)
    if(toroidaln==1):
        for i in xrange(mtoroidal):
	    x.append(2.0*3.141592*float(i)/float(mtoroidal*toroidaln))
    else:
       for k in xrange(toroidaln):
          xtmp=[]
       	  for i in xrange(mtoroidal):
	    xtmp.append(2.0*3.141592*float(i+(k*mtoroidal))/float(mtoroidal*toroidaln))
          x.append(xtmp)
    #ff=f

    zmax=abs(f).max()
    zmin=-abs(f).max()
    xmax=max(x)
    xmin=min(x)
    ymax=3.141592#max(y)
    ymin=-3.141592#min(y)

    flevel=[]  
    clevel=[]
    for j in range(0,ncolor):
        flevel.append(zmin+((zmax-zmin)*j)/(ncolor-1))
    for j in range(0,contourlevels):
        clevel.append(zmin+((zmax-zmin)*j)/(contourlevels-1))
    if nofill=='n':
        if toroidaln==1:
           CS=plt.contourf(x,y,ff,flevel)
        else:
           plt.hold("True")
           for k in xrange(toroidaln):
               CS=plt.contourf(x[k],y,ff,flevel)
    if contourlines=="y":
	plt.hold("True")
        if contourcolors=="k":
    		CS=plt.contour(x,y,ff,clevel,colors='k')
        else:
    		CS=plt.contour(x,y,ff,clevel)
        plt.hold("False")
    if toroidaln==1:
    	plt.xlim([xmin,xmax])
    else:
        plt.xlim([0,2*3.141592])
    plt.ylim([ymin,ymax])
    #FMT=ticker.LogFormatterMathtext("%.1e")
    plt.clabel(CS, fontsize=15, inline=1, fmt="%.1e")
    return

#########################################################################################################
## makesnapshots
##########################################################################
def readsnapshots(folder,savename,numofsnaps=200,snapsize=200,startat=0,datatype=0,ntdiag=1,zeropadding=5):
    """
    readsnapshots() automates readsnapshot() to turn snap.out poloidal data into a pickle file
    folder: where snap#####.out files are located & where pickle file will be saved
    savename: what you want to call the image files
    numofsnaps: end index number of files to look at
    snapsize: number by which serial files are separated
    startat: beginning index number of files to look at
    datatype: 0=first column; 1=second column, 2=third column
    ntdiag: 1 by default
    """
    import time
    xx=[]
    y=[]
    f=[]
    fluxd=[]
    now=time.clock()
    print "Start time=",now 
    print "Now at:" 
    for i in range(startat,numofsnaps):
      #try:
        snapnumname=str((i*snapsize))
        add=zeropadding-len(snapnumname);
        snapnumname=("0"*add)+snapnumname
        if i==0:
            if ntdiag<10:
                snapnumname=("0"*(zeropadding-1))+str(ntdiag)
            elif 10<=ntdiag<100:
                snapnumname=("0"*(zeropadding-2))+str(ntdiag)
        name=folder+"snap"+snapnumname+".out"
        print(name)
        (poloidata,nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)=readsnapshot(name,3)
	(fluxdata,nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)=readsnapshot(name,4)
        if i==startat:
            variables=[nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax]
        xx.append(poloidata[:,:,nfield])
        y.append(poloidata[:,:,nfield+1])
        f.append(poloidata[:,:,datatype])
        #f.append(poloidata[:,:,0])
        fluxd.append(fluxdata[:,:,datatype])
 
        if i%math.ceil(numofsnaps/10.0)==0:
	    message=">>"+str(math.ceil(float(i)*100/numofsnaps))+"%"
            print message,
	if i==numofsnaps-1:
	    message="[[DONE]]"
	    print message

      #except:
        #print "Something went wrong with the snapshot reading at",i,"!!"
    import pickle
    fffu = open(folder+savename+'.pckl', 'w')
    pickle.dump([variables,xx,y,f,fluxd], fffu)
    #pickle.dump([variables,xx,y,f], fffu)
    fffu.close()
    later=time.clock()
    timecost=later-now
    print "End time=", later
    print "Run time=",timecost,"s"
    return
#########################################################################################################
## spectrum
##########################################################################
def spectrum(fluxx,mtgrid,mtoroidal,mmode=20):#,kind=0):
    #kind=0 is poloidal, kind=1 is toroidal
    #if kind=0
        #mmode=20

        x1=range(0,mmode)
        y1=numpy.zeros(mmode)
        yy=numpy.zeros(mtgrid)
        for i in range(0,mtoroidal-1):
            yy=fluxx[:,i]
            yy2=numpy.fft.fft(yy)
            y1[0]=y1[0]+(abs(yy2[0])**2)
            for j in range(1,mmode-1):
                y1[j]=y1[j]+(abs(yy2[j])**2)+(abs(yy2[mtgrid-j])**2)
        for j in range(0,mmode):
	    a=math.sqrt(y1[j]/mtoroidal)
    	    b=a/mtgrid
    	    y1[j]=b

	plt.plot(x1,y1)
        return x1,y1
    #if kind=1
    #    mmode=60
    #
    #    x1=range(0,mmode)
    #    y1=numpy.zeros(mmode)
    #    yy=numpy.zeros(mtgrid)
    #    for i in range(0,mtoroidal-1):
    #        yy=fluxx[:,i]
    #        yy2=numpy.fft.fft(yy)
    #        y1[0]=y1[0]+(abs(yy2[0])**2)
    #        for j in range(1,mmode-1):
    #            y1[j]=y1[j]+(abs(yy2[j])**2)+(abs(yy2[mtgrid-j])**2)
    #    for j in range(0,mmode):
    #	    a=math.sqrt(y1[j]/mtoroidal)
    #	    b=a/mtgrid
    # 	    y1[j]=b
    #
    #	plt.plot(x1,y1)
    #x2=indgen(pmode)
    #y2=fltarr(pmode)*0.0

    #yy=fltarr(mtoroidal)
    #for i in range(0,mtgrid-1):
    #    yy=f(*,i)
    #    yy=fft(yy,1)
    #    y2(0)=y2(0)+(abs(yy(0))**2)
    #    for j in range(1,pmode-1):
    #        y2(j)=y2(j)+(abs(yy(j))**2)+(abs(yy(mtoroidal-j))**2)
    #y2=sqrt(y2/mtgrid)
    #y2=y2/mtoroidal

        
#########################################################################################################
## plotpoloidal
##########################################################################
def plotpoloidal(folder,savename,numofsnaps=200,snapsize=200,startat=0,radialmax=0):
    """
    plotpoloidal() creates png files of poloidal snapshots from pickle file made by readsnapshots()
    folder: where the pickle file is located and where "pics" folder will be located
    savename: what you want to call the image files
    numofsnaps: end index number of files to look at
    snapsize: number by which serial files are separated
    startat: beginning index number of files to look at
    """
    import time
    now=time.clock()
    print "Start time=",now 
    print "Now at:" 
    import pickle
    import os
    try:
        os.stat(folder+"pics/")
    except:
        os.mkdir(folder+"pics/")
    try:
        os.stat(folder+"pics/poloidalsnapshots/")
    except:
        os.mkdir(folder+"pics/poloidalsnapshots/")
    f = open(folder+savename+'.pckl')
    savefile = pickle.load(f)
    f.close()
    (nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)=savefile[0]
    xx=savefile[1]
    yy=savefile[2]
    ff=savefile[3]
    if radialmax!=0:
        radialmax=ff.max()
    ffluxdd=savefile[4]
    figgypi=plt.figure(figsize=(20,10))
    plt.hold(False)
    ##make grids##
    gs=gridspec.GridSpec(3,3)
    for i in range(startat,numofsnaps):

      try:
        snapnumname=str((i*snapsize))
        add=5-len(snapnumname);
        snapnumname=("0"*add)+snapnumname
        if i==0:
            snapnumname="00001"
        x=xx[i]
        y=yy[i]
        f=ff[i]
        fluxd=ffluxdd[i]
      except:
        print "Something went wrong with the files at",i,"!!"

      try:
        #plt.subplot(121)
        plt.subplot(gs[:,0])
        poloidal(x,y,f)
        plt.title("t="+snapnumname,fontsize=15)
      except:
        print "Something went wrong with the poloidal plot at",i,"!!"

      #try:
      plt.subplot(gs[0,1:])
      spectrum(fluxd,mtgrid,mtoroidal)
      plt.title("t="+snapnumname,fontsize=15)
      #except:
        #print "Something went wrong with the spectral plot at",i,"!!"

      #try:
      #plt.subplot(gs[1,1])
      plt.subplot(gs[-1,1:]) 
      polprofile(f,mpsi,mtgrid,radialmax,i)
      plt.title("Poloidal Profile, t="+snapnumname,fontsize=15)
      #except:
        #print "Something went wrong with the poloidal profile plot at",i,"!!"

      #try:
      #plt.subplot(122)
      plt.subplot(gs[-2,1:])
      fluxdata(fluxd,mtoroidal,mtgrid)
      plt.ylabel("theta")
      plt.xlabel("toroidal")
	#radial(f,mpsi,mtgrid,radialmax,i)
        #plt.title("Psi Profile, t="+snapnumname,fontsize=15)

      if i%math.ceil(numofsnaps/10.0)==0:
	    message=">>"+str(math.ceil(float(i)*100/numofsnaps))+"%"
            print message,
      if i==numofsnaps-1:
	    message="[[DONE]]"
	    print message

      #except:
        #print "Something went wrong with the radial plot at",i,"!!"

      plt.savefig(folder+"pics/poloidalsnapshots/"+savename+snapnumname+".png")
      plt.gcf().clear()
    plt.close(figgypi)    
    later=time.clock()
    timecost=later-now
    print "End time=", later
    print "Run time=",timecost,"s"
    return
#########################################################################################################
## plotradialtime
##########################################################################
def plotradialtime(folder,savename,numofsnaps=200,snapsize=200,startat=0):
    """
    plotradialtime() gives you 3 plots of snapshots analyzed by radial position and time.
    folder: where the pickle file is located and where "pics" folder will be located
    savename: what you want to call the image files
    numofsnaps: end index number of files to look at
    snapsize: number by which serial files are separated
    startat: beginning index number of files to look at
    """
    import time
    now=time.clock()
    print "Start time=",now 
    import pickle
    import os
    try:
        os.stat(folder+"pics/")
    except:
        os.mkdir(folder+"pics/")  
    f = open(folder+savename+'.pckl')
    savefile = pickle.load(f)
    f.close()
    (nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)=savefile[0]
    ff=savefile[3]
    t=range(0,numofsnaps)
    r=range(0,mpsi)
    ffrt=numpy.zeros((mpsi,numofsnaps))
    adjustedffrt=numpy.zeros((mpsi,numofsnaps))
    normffrt=numpy.zeros((mpsi,numofsnaps))
    
    for i in range(startat,numofsnaps):
        f=ff[i]
        ffrt[:,i]=radial(f,mpsi,mtgrid,1)
    for i in range(0,mpsi):
        adjustedffrt[i,:]=ffrt[i,:]/max(ffrt[i,:])  
    for i in range(0,numofsnaps):
        normffrt[:,i]=ffrt[:,i]/max(ffrt[:,i])         
    
    figgypi=plt.figure(figsize=(20,15))
    
    plt.subplot(3,1,1)
    plt.contourf(t,r,ffrt,12)    
    plt.colorbar()
    plt.ylabel("Psi")
    plt.xlabel("Time")
    plt.title("Radial-Time")
    
    plt.subplot(3,1,2)
    plt.contourf(t,r,adjustedffrt,12)    
    plt.colorbar()
    plt.ylabel("Psi")
    plt.xlabel("Time")
    plt.title("Radial-Time, Normalized Across Time")
    
    plt.subplot(3,1,3)
    plt.contourf(t,r,normffrt,12)    
    plt.colorbar()
    plt.ylabel("Psi")
    plt.xlabel("Time")
    plt.title("Radial-Time, Normalized Across Radial")
    
    plt.savefig(folder+"pics/"+savename+"_radialtime_"+".png")
    later=time.clock()
    timecost=later-now
    print "End time=", later
    print "Run time=",timecost,"s"
    return [ffrt,t,r]

