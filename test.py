# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'test.ui'
#
# Created: Wed May 28 19:29:58 2014
#      by: PyQt4 UI code generator 4.9.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(500, 303)
        self.delete_pngs_after = QtGui.QRadioButton(Dialog)
        self.delete_pngs_after.setGeometry(QtCore.QRect(340, 270, 143, 21))
        self.delete_pngs_after.setChecked(True)
        self.delete_pngs_after.setObjectName(_fromUtf8("delete_pngs_after"))
        self.folder = QtGui.QLineEdit(Dialog)
        self.folder.setGeometry(QtCore.QRect(10, 101, 301, 25))
        self.folder.setProperty("Folder Path", _fromUtf8(""))
        self.folder.setObjectName(_fromUtf8("folder"))
        self.modulepath = QtGui.QLineEdit(Dialog)
        self.modulepath.setGeometry(QtCore.QRect(10, 50, 301, 25))
        self.modulepath.setObjectName(_fromUtf8("modulepath"))
        self.label_6 = QtGui.QLabel(Dialog)
        self.label_6.setGeometry(QtCore.QRect(10, 30, 166, 15))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.groupbox = QtGui.QGroupBox(Dialog)
        self.groupbox.setGeometry(QtCore.QRect(330, 20, 143, 251))
        self.groupbox.setObjectName(_fromUtf8("groupbox"))
        self.startat = QtGui.QSpinBox(self.groupbox)
        self.startat.setGeometry(QtCore.QRect(10, 170, 52, 25))
        self.startat.setObjectName(_fromUtf8("startat"))
        self.size = QtGui.QSpinBox(self.groupbox)
        self.size.setGeometry(QtCore.QRect(10, 120, 52, 25))
        self.size.setObjectName(_fromUtf8("size"))
        self.num = QtGui.QSpinBox(self.groupbox)
        self.num.setGeometry(QtCore.QRect(10, 70, 52, 25))
        self.num.setObjectName(_fromUtf8("num"))
        self.label_12 = QtGui.QLabel(self.groupbox)
        self.label_12.setGeometry(QtCore.QRect(10, 200, 58, 15))
        self.label_12.setObjectName(_fromUtf8("label_12"))
        self.ntdiag = QtGui.QSpinBox(self.groupbox)
        self.ntdiag.setGeometry(QtCore.QRect(10, 220, 52, 25))
        self.ntdiag.setObjectName(_fromUtf8("ntdiag"))
        self.label_3 = QtGui.QLabel(self.groupbox)
        self.label_3.setGeometry(QtCore.QRect(11, 53, 130, 16))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.savename = QtGui.QLineEdit(self.groupbox)
        self.savename.setGeometry(QtCore.QRect(11, 22, 129, 25))
        self.savename.setObjectName(_fromUtf8("savename"))
        self.label_5 = QtGui.QLabel(self.groupbox)
        self.label_5.setGeometry(QtCore.QRect(11, 157, 102, 16))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.label_4 = QtGui.QLabel(self.groupbox)
        self.label_4.setGeometry(QtCore.QRect(11, 105, 66, 16))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.label_2 = QtGui.QLabel(self.groupbox)
        self.label_2.setGeometry(QtCore.QRect(11, 1, 68, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(10, 80, 166, 15))
        self.label.setObjectName(_fromUtf8("label"))
        self.progressBar = QtGui.QProgressBar(Dialog)
        self.progressBar.setGeometry(QtCore.QRect(20, 270, 118, 23))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.begin_program = QtGui.QPushButton(Dialog)
        self.begin_program.setGeometry(QtCore.QRect(20, 230, 87, 27))
        self.begin_program.setObjectName(_fromUtf8("begin_program"))
        self.datatype = QtGui.QSpinBox(Dialog)
        self.datatype.setGeometry(QtCore.QRect(250, 170, 52, 25))
        self.datatype.setObjectName(_fromUtf8("datatype"))
        self.textBrowser = QtGui.QTextBrowser(Dialog)
        self.textBrowser.setGeometry(QtCore.QRect(170, 140, 151, 71))
        self.textBrowser.setObjectName(_fromUtf8("textBrowser"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.delete_pngs_after.setText(QtGui.QApplication.translate("Dialog", "Delete PNGS after ", None, QtGui.QApplication.UnicodeUTF8))
        self.folder.setText(QtGui.QApplication.translate("Dialog", "\'/data/users/lauc2/snap_clau/\'", None, QtGui.QApplication.UnicodeUTF8))
        self.modulepath.setText(QtGui.QApplication.translate("Dialog", "\"/data/users/lauc2/data-analysis/gtc_tools/\"", None, QtGui.QApplication.UnicodeUTF8))
        self.label_6.setText(QtGui.QApplication.translate("Dialog", "SnapshotGraphics Path:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_12.setText(QtGui.QApplication.translate("Dialog", "ntdiag:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("Dialog", "Number of Data Files:", None, QtGui.QApplication.UnicodeUTF8))
        self.savename.setText(QtGui.QApplication.translate("Dialog", "\'deltane\'", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("Dialog", "Start at Number:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("Dialog", "Step Sizes:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Dialog", "Savename:", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Dialog", "Folder Path:", None, QtGui.QApplication.UnicodeUTF8))
        self.begin_program.setText(QtGui.QApplication.translate("Dialog", "Run!", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser.setHtml(QtGui.QApplication.translate("Dialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Sans\'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Column of Interest:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">  0 - Phi</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">  1 - Apara</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">  2 - Fluidne</p></body></html>", None, QtGui.QApplication.UnicodeUTF8))

