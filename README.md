=====================================================================

04/01/2014-- (C.Lau) Added the HistoryAnalysis.py module file.

03/15/2014-- (C.Lau) Added a README file with basic instructions.

=====================================================================

General Comments
========================
These modules are based on data analysis codes previously written in IDL by [Lin et al](http://phoenix.ps.uci.edu/zlin/) for our particle-in-cell code, [GTC](http://phoenix.ps.uci.edu/gtc/). I translated these into python and have since added updates for quicker and more automated 
analysis. I wrote and use these modules in IPython, and I plan to add in a GUI for the scripts in the near future. To use these, remember to copy the modules into a path that python can see, and to 
load them in your python session. If you have any comments or questions, please contact me at *calvin.lau@uci.edu*.

HistoryAnalysis
========================

This python module contains functions to read and analyze data from GTC runs, specifically from the *history.out* files. At the moment, unfinished runs can't be read by this so a quick-fix would be to allow GTC
to run for 1 tstep really quickly before running the HistoryAnalysis scripts.

First, remember to load the module.

## import HistoryAnalysis 

    import HistoryAnalysis

# Files

Here are the scripts involved. 

## read_mode_history
    
    def read_mode_history(fname,typehist="mode"):
		....
		if typehist=="particle":
			....
			return parthist,tstep
		if typehist=="field":
			....
			return fieldhist,tstep
		if typehist=="mode":
			....
			return modehist,tstep

This function reads the history.out file as designated by the string "fname" (should include filedirectory if you aren't already in that directory). The option "typehist" is referring the possible things
that one might want to look at in the GTC output, the mode history ("mode"), the particle history ("particle"), and the field history ("field"). It then returns the "tstep" (size of one time step in the
simulation in GTC units of $R_0/C_s$) and different sized arrays depending on which "typehist" was chosen:

	parthist[ndstep,mpdiag,nspecies]
	fieldhist[ndstep,mfdiag,nfield]
	modehist[nstep,nspecies,modes,nfield]

## simple_smooth

    def simple_smooth(y,n_frac=0.05):
		....
    	return z

This function just does a simple smooth (as indicated by the name...) on the input 1-D array "y" and smooths it based on the "n_frac" which indicates what fraction of the total number of points is considered
to be neighbors considered in the smooth. The output 1-D array "z" is a reduced length array, reduced by twice the fraction determined by "n_frac" as those do not have enough neighboring points.

## find_period

    def find_period(index):
        ....
        return numpy.mean(temp),temp

This function is called in another function as one of two methods of finding the frequency. It is used to find the lowest (or lower) period using the input "index". The first output is the average period and the second output is the set of periods used to find that average.

## freq_and_gamma

    def freq_and_gamma(modehist,modenum,tstep,nstart_frac=0,nend_frac=1,plot_figures="hide",smoothnum=0.05,freqcalc="counting",fftsteps=1.0,fftsign="positive"):
        ....
        return gamma,freq,periods,numpy.std(periods)

This function calculates the frequency and growth rate of the "modehist" by using either a fast fourier transform method or by counting the peaks for frequency (chosen by assigning "freqcalc" as either "fft" or "counting", and also choosing whether to look for positive or negative frequencies, ie. electron or ion branches) and by using a linear fit via least-squares on the exponential growth of the "modehist" for gamma. The parameters "nstart_frac" and "nend_frac" determine where you want the frequency and growth rate to be calculated from so you can avoid the messy initial stages before the instability grows. This function also produces figures, showing the growth rate ("plot_figures"="gamma"), frequency ("plot_figures"="freq"), or none at all ("plot_figures"="hide") so you can eyeball whether your range should change. If there are unwanted high frequencies, they can smoothed away by setting a value for "smoothnum" which indicates the fraction of the total steps being used as neighboring points for smoothing. Finally, you need to choose a "modenum" which is according to the 8 modes kept by GTC runs. In the future, I would like to implement a change in GTC that allows a variable number of modes instead a static 8, but for now, "modenum" goes from 1 to 8. 

# Usage Example



SnapshotGraphics
========================

## readsnapshot

def readsnapshot(fname,kind=0): 
    ....
    if kind==0:
        return (profile,pdf,poloidata,fluxdata,nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)
    elif kind==1:
        return (profile,nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)
    elif kind==2:
        return (pdf,nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)
    elif kind==3:
        return (poloidata,nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)
    elif kind==4:
        return (fluxdata,nspecies,nfield,nvgrid,mpsi,mtgrid,mtoroidal,tmax)

## radialprof

def radialprof(f,mpsi,mtgrid,plott=0,dots=1):
    ....
    return y2

##

def radial(f,mpsi,mtgrid,plott=0,dots=1):
    ....

    return y2

# Additional Comments: 


* I have not implemented a GUI yet. I hope to do it soon one day.

* It will be greatly appreciated if you want to contribute. Please feel free to contact me at *calvin.lau@uci.edu*

# License:

This is a free-software available under GNU General Public License Version 3 (see LICENSE.md).