import sys
from PyQt4 import QtCore, QtGui
from test import Ui_Dialog

class StartQT4(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
	self.ui.begin_program.clicked.connect(self.run_mee)
    def run_mee(self):
	import sys,SnapshotGraphics,pylab,matplotlib
	sys.path.append(self.ui.modulepath)

	folder=self.ui.folder.text()
	savename=self.ui.savename.text()
	size=self.ui.size.value()
	startat=self.ui.startat.value()
	num=self.ui.num.value()
	datatype=self.ui.datatype.value()
	ntdiag=self.ui.ntdiag.value()
	#print size
	#print type(size)
	#print folder
	#print type(folder)
	#ntdiag=self.ui.ntdiag
	#matplotlib.pyplot.set_cmap('RdBu')
	SnapshotGraphics.readsnapshots(folder,savename,num,size,startat,datatype,ntdiag)
	SnapshotGraphics.plotpoloidal(folder,savename,num,size,startat,0)
	#import subprocess
	#subprocess.Popen("cwm --rdf test.rdf --ntriples > test.nt")

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = StartQT4()
    myapp.show()
    sys.exit(app.exec_())
