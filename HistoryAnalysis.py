# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>
# <codecell>

#fdirectory="C:/Users/Calvin/Documents/C2--Conv_Test/base_run/"
#fname= fdirectory + 'history.out'

def read_mode_history(fname,typehist="mode",printstuff='no'):
    import numpy
    lines = open(fname, 'r').readlines()
    ndstep=int(lines[0])   # of time steps
    nspecies=int(lines[1]) # of species: ion, electron, EP, impuries
    mpdiag=int(lines[2])   # of quantities per species: density,entropy,momentum,energy, fluxes
    nfield=int(lines[3])   # of field variables: phi, a_par, fluidne
    modes=int(lines[4])    # of modes per field: (n,m)
    mfdiag=int(lines[5])   # of quantities per field: rms, single spatial point, zonal components
    tstep=float(lines[6])  # time step size

    if printstuff=='yes':
       print "# tsteps","# species", "# diagnostics", "# fields", "# modes", "# field quants", "tstep size"
       print ndstep, nspecies, mpdiag, nfield, modes, mfdiag, tstep
    #total_len=len(lines[7:]) # first 7 variables are used to make later array sizes
    total_len=ndstep*((mpdiag*nspecies)+(mfdiag*nfield)+(modes*nfield*2))
    #print "# total len", "# len of lines", "# tsteps"
    #print total_len, len(lines[7:]), ndstep
    if (total_len-len(lines[7:])!=0):
	    total_len=len(lines[7:])
	    ndstep=total_len/((mpdiag*nspecies)+(mfdiag*nfield)+(modes*nfield*2))
    #print "# total len", "# len of lines", "# tsteps"
    #print total_len, len(lines[7:]), ndstep
    ndstep_len=total_len/ndstep
    hist_len=[(mpdiag*nspecies),(mfdiag*nfield),(modes*nfield*2)]
    history=numpy.zeros(total_len)
    hist_separated=numpy.zeros((ndstep,ndstep_len))

    partdata=numpy.zeros((ndstep,hist_len[0]))
    fielddata=numpy.zeros((ndstep,hist_len[1]))
    modedata=numpy.zeros((ndstep,hist_len[2]))

    parthist=numpy.zeros((ndstep,mpdiag,nspecies))
    fieldhist=numpy.zeros((ndstep,mfdiag,nfield))
    modehist=numpy.zeros((ndstep,2,modes,nfield))

    for i in range(0,total_len):
        history[i]=float(lines[7+i]) 
    for j in range(0,ndstep):
        for i in range(0,ndstep_len):
            hist_separated[j,i]=history[(j*ndstep_len)+i]
    for it in range(0,ndstep):
	partdata[it,0:hist_len[0]]=hist_separated[it,0:hist_len[0]]
        fielddata[it,0:hist_len[1]]=hist_separated[it,hist_len[0]:(hist_len[1]+hist_len[0])]
	#print(hist_len[0],hist_len[1],hist_len[2],len(hist_separated[0,:]))
        modedata[it,0:len(hist_separated[0,:])-hist_len[0]-hist_len[1]]=hist_separated[it,(hist_len[1]+hist_len[0]):len(hist_separated[0,:])]#(hist_len[0]+hist_len[1]+hist_len[2])]

	#partdata[it,:]=hist_separated[it,0:hist_len[0]]
        #fielddata[it,:]=hist_separated[it,hist_len[0]:(hist_len[1]+hist_len[0])]
        #modedata[it,:]=hist_separated[it,(hist_len[1]+hist_len[0]):(hist_len[0]+hist_len[1]+hist_len[2])]

    if typehist=="particle":
        print(len(partdata[1,:]),ndstep,mpdiag,nspecies)
        for it in range(0,ndstep):
            for k in range(0,nspecies):
                for j in range(0,mpdiag):
                    rightnow=((k*mpdiag)+j)
                    parthist[it,j,k]=partdata[it,rightnow]
        return parthist,tstep
    elif typehist=="field":
        for it in range(0,ndstep):
            for k in range(0,nfield):
                for j in range(0,mfdiag):
                    rightnow=((k*mfdiag)+j)
		    fieldhist[it,j,k]=fielddata[it,rightnow]
        return fieldhist,tstep
    elif typehist=="mode":
        for it in range(0,ndstep):
            for k in range(0,nfield):
                for j in range(0,modes):
                    rightnow=((k*modes)+j)*2
                    modehist[it,0,j,k]=modedata[it,rightnow]
                    modehist[it,1,j,k]=modedata[it,rightnow+1]
        return modehist,tstep
    
##############################

def simple_smooth(y,n_frac=0.05):
    length=len(y)
    n=int(len(y)*n_frac)
    z=[]
    for i in xrange(n,len(y)-n):
        temp=0
        for j in xrange(-n,n+1):
            temp=temp+y[i+j]
        z.append(temp/((2*n)+1))
    return z

##############################

def zeros_of_diff(y,x,n_frac=0.05):
    import numpy
    yy=simple_smooth(y,n_frac)
    diffy=numpy.diff(yy)
    length=len(diffy)-1
    index_offset=int(len(y)*n_frac)
    z=[]
    zindex=[]
    for i in xrange(length):
        temp=numpy.sign(diffy[i])*numpy.sign(diffy[i+1])
        if temp < 0:
            z.append(y[i+index_offset])
            zindex.append(x[i+index_offset])
    return z, zindex

##############################

def find_period(index):
    import numpy
    length=len(index)-1
    temp=[]
    temp2=[]
    for i in xrange(length):
        ## times 2 because these are half periods
        temp.append(2*(index[i+1]-index[i]))
    wavg=numpy.mean(temp)*0.5
    length2=len(temp)
    for i in xrange(length2):
        if temp[i]>wavg:
            temp2.append(temp[i])
    wavg=numpy.mean(temp2)*0.9
    length=len(temp2)
    temp=[]
    for i in xrange(length):
        if temp2[i]>wavg:
            temp.append(temp2[i])
    return numpy.mean(temp),temp

############################### 

def freq_and_gamma(modehist,modenum,tstep,
		nstart_frac=0,nend_frac=1,
		plot_figures="hide",smoothnum=0.05,
		freqcalc="counting",fftsteps=1.0,fftsign="positive",
		typehist="mode"):
    import numpy
    import matplotlib.pyplot as plt
    ##### MODE HISTORY #####
    if typehist=="mode":
        mnum=modenum-1
        x1=modehist[:,0,mnum,0]
        x2=modehist[:,1,mnum,0]
    elif typehist=="rms":
	x1=modehist[:,2,0]
	x2=modehist[:,3,0]
    elif typehist=="phi00":
	x1=modehist[:,0,0]
	x2=modehist[:,1,0]
    nstart=int(nstart_frac*len(x1))
    nend=int(nend_frac*len(x1))
    yrall=x1[nstart:nend]
    yiall=x2[nstart:nend]
    xx=[]
    for i in xrange(nstart,nend):
        xx.append(i)

    ##### MODE AMPLITUDE #####

    ypow_all=[]
    ya_all=[]
    yr_norm=[]
    yi_norm=[]
    y_complex_norm=[]
    for i in xrange(nend-nstart):
        temp=((yrall[i]*yrall[i])+(yiall[i]*yiall[i]))**0.5
        ya_all.append(temp)
        ypow_all.append(numpy.log(temp))
        yr_norm.append(yrall[i]/temp)
        yi_norm.append(yiall[i]/temp)
        y_complex_norm.append(complex(yrall[i]/temp,yiall[i]/temp))
    ypow_all_original=[]
    xx_original=[]
    for i in xrange(len(x2)):
        temp=((x1[i]*x1[i])+(x2[i]*x2[i]))**0.5
        ypow_all_original.append(numpy.log(temp))
	xx_original.append(i)
    ##### LINEAR GROWTH GAMMA #####
    
    ypow_all_smoothed=simple_smooth(ypow_all,smoothnum)
    #print(len(ypow_all_smoothed),len(ypow_all))
    templen=(len(ypow_all)-len(ypow_all_smoothed))/2
    xx_smoothed=xx[templen:len(xx)-templen]
    #print(templen,len(xx_smoothed),len(xx))
    linfit=numpy.polyfit(xx_smoothed,ypow_all_smoothed,1)
    gamma=linfit[0]/tstep
    yfit=[]
    #gamma=(log(ya_all[nend-nstart]/ya_all[0])/(nend-nstart))/tstep
    for i in range(nstart,nend):
        yfit.append((linfit[0]*i)+linfit[1])
        
    #### FREQUENCY ####
    
    [z,zind]=zeros_of_diff(yr_norm,xx,smoothnum)
    [period,periods]=find_period(zind)
    if freqcalc=="counting":
        freq=1.0/period/tstep#/utime #now in units of utime
    elif freqcalc=="fft":
        y_complex_smoothed=simple_smooth(y_complex_norm,smoothnum)
        y_complex_norm=y_complex_smoothed
        freq_fft=abs(numpy.fft.fft(y_complex_norm))
	#freq_interval=numpy.fft.fftfreq(numpy.linspace(tstep*nstart,tstep*nend,len(freq_fft)).shape[-1])
        freq_interval=numpy.fft.fftfreq(numpy.linspace(nstart,nend,len(freq_fft)).shape[-1])
        for ii in xrange(len(freq_interval)):
            freq_interval[ii]=freq_interval[ii]/tstep
        if fftsign=="positive":
		freq=freq_interval[numpy.where(abs(freq_fft)==numpy.amax(abs(freq_fft[numpy.where(freq_interval>0)])))]
        elif fftsign=="negative":
		freq=freq_interval[numpy.where(abs(freq_fft)==numpy.amax(abs(freq_fft[numpy.where(freq_interval<0)])))]
        elif fftsign=="both":
		freq=freq_interval[numpy.where(abs(freq_fft)==numpy.amax(abs(freq_fft[numpy.where((max(freq_interval)*fftsteps)>freq_interval>(min(freq_interval)*fftsteps))])))]
        elif fftsign=="either":
                temppositive=freq_interval[numpy.where(abs(freq_fft)==numpy.amax(abs(freq_fft[numpy.where(freq_interval>0)])))]
                tempnegative=freq_interval[numpy.where(abs(freq_fft)==numpy.amax(abs(freq_fft[numpy.where(freq_interval<0)])))]
                if abs(temppositive)>abs(tempnegative):
                   freq=temppositive
                else:
                   freq=tempnegative
    elif freqcalc=="both":
        y_complex_smoothed=simple_smooth(y_complex_norm,smoothnum)
        y_complex_norm=y_complex_smoothed
        freq_fft=abs(numpy.fft.fft(y_complex_norm))
	#freq_interval=numpy.fft.fftfreq(numpy.linspace(tstep*nstart,tstep*nend,len(freq_fft)).shape[-1])
        freq_interval=numpy.fft.fftfreq(numpy.linspace(nstart,nend,len(freq_fft)).shape[-1])
        for ii in xrange(len(freq_interval)):
            freq_interval[ii]=freq_interval[ii]/tstep
        if fftsign=="positive":
		freq2=freq_interval[numpy.where(abs(freq_fft)==numpy.amax(abs(freq_fft[numpy.where(freq_interval>0)])))]
        elif fftsign=="negative":
		freq2=freq_interval[numpy.where(abs(freq_fft)==numpy.amax(abs(freq_fft[numpy.where(freq_interval<0)])))]
        elif fftsign=="both":
		freq2=freq_interval[numpy.where(abs(freq_fft)==numpy.amax(abs(freq_fft[numpy.where((max(freq_interval)*fftsteps)>freq_interval>(min(freq_interval)*fftsteps))])))]
	freq1=1.0/period/tstep
	freq=[freq1,freq2]
        #elif fftsign=="either":
#		temppositive=freq_interval[numpy.where(abs(freq_fft)==numpy.amax(abs(freq_fft[numpy.where(freq_interval>0)])))]
#                tempnegative=freq_interval[numpy.where(abs(freq_fft)==numpy.amax(abs(freq_fft[numpy.where(freq_interval<0)])))]
#                if abs(temppositive)>abs(tempnegative):
#                   freq2=temppositive
#                else:
#                   freq2=tempnegative
    ##### PLOTTING #####
    
    if plot_figures=="show":
        plt.figure(figsize=(20,10))
        #xkcd()
        plt.subplot(221)
        plt.plot(xx,yrall,'r')
        plt.plot(xx,yiall,'b')
        plt.xlim([nstart,nend])
        plt.ylim([min(yrall),max(yrall)])
        plt.xlabel("t [Unit=1.0*C_s/R_0]")
        plt.legend(["Real","Imag."],loc=2)
        plt.title('Mode History')

        plt.subplot(222)
        plt.plot(xx,ypow_all,'r')
        plt.xlim([nstart,nend])
        #plt.ylim([min(ya_all),max(ya_all)])
        plt.xlabel("t [Unit=1.0*C_s/R_0]")
        plt.title('log(Mode Amplitude), gamma = '+str(round(gamma,5)))

        plt.subplot(223)
        plt.plot(xx,yr_norm,'r')
        plt.plot(xx,yi_norm,'b')
        plt.xlim([nstart,nend])
        plt.ylim([1.5*min(yr_norm),1.5*max(yr_norm)])
        plt.xlabel("t [Unit=1.0*C_s/R_0]")
        plt.legend(["Real","Imag."],loc=2)
        plt.title('Normalized Mode History ~ freq = '+str(round(freq,5)))
    
        plt.subplot(224)
        plt.plot(xx,yr_norm)
        for i in xrange(len(zind)):
            plt.plot(zind[i],z[i],'o')
        plt.ylim([1.5*min(yr_norm),1.5*max(yr_norm)])
        plt.xlim([nstart,nend])
        plt.xlabel("t [Unit=1.0*C_s/R_0]")
        plt.legend(["Real"],loc=2)
        plt.title('Check Freq- Norm. Mode Hist. ~ '+'stdev of periods = ' + str(round(numpy.std(periods),3)))
        
    elif plot_figures=="gamma":
        #plt.figure(figsize(10,5))
        #plt.plot(xx,ypow_all,'r')
	plt.plot(xx_original,ypow_all_original,'r')
	plt.plot(xx,yfit,'b--')
	#plt.xlim([nstart,nend])
        #plt.ylim([min(ya_all),max(ya_all)])
        #plt.xlabel("t [Unit=1.0*C_s/R_0]")
        #plt.title('log(Mode Amplitude), gamma = '+str(round(gamma,5)))
    
    elif plot_figures=="freq":
        #plt.figure(figsize(10,5))
	if freqcalc=="counting":
            plt.plot(xx,yr_norm)
            for i in xrange(len(zind)):
                plt.plot(zind[i],z[i],'o')
            plt.ylim([1.5*min(yr_norm),1.5*max(yr_norm)])
            plt.xlim([nstart,nend])
	elif freqcalc=="fft":
            plt.plot(freq_interval,abs(freq_fft))
            xtickers=numpy.linspace(min(freq_interval)*fftsteps,max(freq_interval)*fftsteps, 3)
            for ii in xrange(len(xtickers)):
                xtickers[ii]=round(xtickers[ii],2)
            plt.xticks(xtickers)
            plt.xlim([min(freq_interval)*fftsteps,max(freq_interval)*fftsteps])
        #plt.xlabel("t [Unit=1.0*C_s/R_0]")
        #plt.legend(["Real"],loc=2)
        #plt.title('Check Freq- Norm. Mode Hist. ~ '+'stdev of periods = ' + str(round(std(periods),3)))
        
    elif plot_figures=="hide":
        pass
    
    return gamma,freq,periods,numpy.std(periods)#,yr_norm,xx

